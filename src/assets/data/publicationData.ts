import { Publication } from 'src/types/publication';

export enum SETS {
  VARIA="Varia",
}

export const publications: Publication[] = [
  {
    name: 'Mortal Fog',
    href: 'https://www.guildhousegamesllc.com/lore-divpal-mortal-fog',
    date: new Date('2020-12-10'),
    desc: 'A noble crusade turns dark, as a paladin\'s faith is put to the test. Part 1 of 3.',
    imageUrl: './assets/img/destin.png',
  set: SETS.VARIA,
  },
  {
    name: 'Skeleton Crew',
    href: 'https://www.guildhousegamesllc.com/lore-dthpir-skeleton-crew',
    date: new Date('2021-01-28'),
    desc: 'A young stowaway gets more adventure than she bargained for. Part 2 of 3.',
    imageUrl: './assets/img/rose.png',
    set: SETS.VARIA,
  },
  {
    name: 'The Ivory Shore',
    href: 'https://www.guildhousegamesllc.com/lore-palpir-theivoryshore',
    date: new Date('2021-05-06'),
    desc: 'Wills and magic clash on a battlefield of bone and brine. Part 3 of 3.',
    imageUrl: './assets/img/destin-v-rose.png',
    set: SETS.VARIA,
  }
]
