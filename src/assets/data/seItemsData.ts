import { Publication } from 'src/types/publication';

export const seItems: Publication[] = [
  {
    name: 'GitLab',
    desc: 'Where all my hairbrained projects live (including this one)!',
    href: 'https://gitlab.com/random72guy',
    matIcon: 'code',
  },
  {
    name: 'Introduction to Web Development',
    desc: 'For beginners who may be interested in becoming a web developer. Provides an overview of the domain, learning resources, tools, tips, and techniques.',
    href: './assets/files/web-dev-intro.pdf',
    matIcon: 'developer_board',
  },
  {
    name: 'Jumping to Ri with Kaizen (Agile Essay)',
    desc: `Hubris breaks many Agile teams. Here's what they missed.`,
    href: './assets/files/jumping-to-ri-with-kaizen.pdf',
    matIcon: 'arrow_upward',
  },
  {
    name: 'The Mule and the Boardwalk (Agile Essay)',
    desc: `The best Scrum adaptation you might be missing!`,
    href: './assets/files/the-mule-and-the-boardwalk.pdf',
    matIcon: 'article',
  },
  {
    name: 'Applying TDD (Agile Essay)',
    desc: `Is Test-Driven Development just an unachievable ideal?`,
    href: './assets/files/applying-tdd.pdf',
    matIcon: 'warning',
  },
  {
    name: 'DevOps Toolchain Selection Guide',
    desc: 'Your guide to choosing the right DevOps tools for your organization.',
    href: './assets/files/devops-toolchain-selection-guide.pdf',
    matIcon: 'construction',
  },
  {
    name: 'Local Pattern',
    desc: 'A software pattern for ensuring client/server API consistency.',
    href: './assets/files/local-pattern.pdf',
    matIcon: 'article',
  },
]

export const professionalItems: Publication[] = [
  {
    name: 'LinkedIn',
    href: 'https://www.linkedin.com/in/christopher-backofen-950705219/',
    desc: 'Very exciting. Wow.',
    matIcon: 'group',
  },
  {
    name: 'Resume',
    href: 'http://chris.backofen.us/resume',
    desc: `Yes, it's up in the corner, too. (It's also a web app!)`,
    matIcon: 'description',
  },
]

export const externalItems: Publication[] = [
  {
    name: 'Lean Essay: Train-Wreck Management',
    href: 'http://www.leanessays.com/2007/09/train-wreck-management.html?m=1',
    desc: 'How US management is optimized for punishing robots rather than enabling thought-workers. An excellent Poppendieck paper.',
    matIcon: 'article',
  },
  {
    name: 'State of JS',
    href: 'https://stateofjs.com/',
    desc: 'Is your software stack cool, or are you behind the times?',
    matIcon: 'article',
  },
  {
    name: 'Periodic Table of DevOps',
    href: 'https://digital.ai/periodic-table-of-devops-tools',
    desc: 'Visualizes the vast ecosystem of DevOps tools.',
    matIcon: 'grid_on',
  },
  {
    name: 'CNCF Cloud Native Interactive Landscape',
    href: 'https://landscape.cncf.io/',
    desc: 'ALSO visualizes the vast ecosystem of DevOps tools.',
    matIcon: 'grid_on',
  },

]
