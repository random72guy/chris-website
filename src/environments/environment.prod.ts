import { Environment } from "src/types/environment";

export const environment: Environment = {
  production: true,
  resumeUrl: 'https://random72guy.gitlab.io/resume/',
};
