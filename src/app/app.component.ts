import { Component } from '@angular/core';
import { environment } from 'src/environments/environment';
import { MediaObserver } from '@angular/flex-layout';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public links: Link[] = [
    new Link('Software Engineering', 'software-engineering', 'code'),
    new Link('Writing', 'writing', 'menu_book'),
  ];

  constructor(
    public mediaObserver: MediaObserver,
  ) { }

  public openResume() {
    window.open(environment.resumeUrl, '_blank');
  }
}

class Link {
  constructor(
    public label: string,
    public routerLink: string,
    public matIcon: string,
  ){}
}
