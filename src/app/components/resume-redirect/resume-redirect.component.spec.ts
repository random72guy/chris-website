import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumeRedirectComponent } from './resume-redirect.component';

describe('ResumeRedirectComponent', () => {
  let component: ResumeRedirectComponent;
  let fixture: ComponentFixture<ResumeRedirectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResumeRedirectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResumeRedirectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
