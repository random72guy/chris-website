import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-resume-redirect',
  templateUrl: './resume-redirect.component.html',
  styleUrls: ['./resume-redirect.component.scss']
})
export class ResumeRedirectComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    window.location.href = environment.resumeUrl, '_blank';
  }

}
