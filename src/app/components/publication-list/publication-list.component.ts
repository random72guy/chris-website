import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { Publication } from 'src/types/publication';
import { orderBy } from 'lodash';

@Component({
  selector: 'app-publication-list',
  templateUrl: './publication-list.component.html',
  styleUrls: ['./publication-list.component.scss']
})
export class PublicationListComponent implements OnInit {

  @Input() publications!: Publication[];
  @Input() orderBy: keyof Publication = 'date';
  @Input() layoutDir: 'row'|'column' = 'row';

  public orderedPublications: Publication[] = [];

  constructor() { }

  ngOnInit(): void {

  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.publications || changes.orderBy) this.updateOrderedPublications();
  }

  private updateOrderedPublications() {
    this.orderedPublications = orderBy(this.publications, p => p[this.orderBy]);
  }

}
