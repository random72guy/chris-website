import { Component, OnInit } from '@angular/core';
import { Publication } from 'src/types/publication';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public links: Publication[] = [
    {
      name: 'Software Engineering',
      routerLink: '/software-engineering',
      desc: 'Essays, links, and professional info!',
      matIcon: 'code',
    },
    {
      name: 'Creative Writing',
      routerLink: '/writing',
      desc: `Shiny charts, stuff to read, and more!`,
      matIcon: 'menu_book',
    }
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
