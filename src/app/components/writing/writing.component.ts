import { Component, OnInit } from '@angular/core';
import { publications } from 'src/assets/data/publicationData';

@Component({
  selector: 'app-writing',
  templateUrl: './writing.component.html',
  styleUrls: ['./writing.component.scss']
})
export class WritingComponent implements OnInit {

  public publications = publications;

  constructor() { }

  ngOnInit(): void {
  }

}
