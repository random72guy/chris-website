import { Component, OnInit } from '@angular/core';
import { externalItems, professionalItems, seItems } from 'src/assets/data/seItemsData';

@Component({
  selector: 'app-software-engineering',
  templateUrl: './software-engineering.component.html',
  styleUrls: ['./software-engineering.component.scss']
})
export class SoftwareEngineeringComponent implements OnInit {
  public items = seItems;
  public professionalItems = professionalItems;
  public externalItems = externalItems;

  constructor() { }

  ngOnInit(): void {
  }

}
