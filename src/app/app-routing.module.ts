import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ResumeRedirectComponent } from './components/resume-redirect/resume-redirect.component';
import { SoftwareEngineeringComponent } from './components/software-engineering/software-engineering.component';
import { WritingComponent } from './components/writing/writing.component';

const routes: Routes = [
  {
    path: 'writing',
    component: WritingComponent
  },
  {
    path: 'resume',
    component: ResumeRedirectComponent
  },
  {
    path: 'software-engineering',
    component: SoftwareEngineeringComponent,
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: '**',
    redirectTo: 'home',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true,
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
