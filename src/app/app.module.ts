import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatLegacyButtonModule as MatButtonModule } from '@angular/material/legacy-button';
import { MatLegacyCardModule as MatCardModule } from '@angular/material/legacy-card';
import { MatIconModule } from '@angular/material/icon';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { WritingComponent } from './components/writing/writing.component';
import { PublicationComponent } from './components/publication/publication.component';
import { ResumeRedirectComponent } from './components/resume-redirect/resume-redirect.component';
import { SoftwareEngineeringComponent } from './components/software-engineering/software-engineering.component';
import { HomeComponent } from './components/home/home.component';
import { PublicationListComponent } from './components/publication-list/publication-list.component';

@NgModule({
  declarations: [
    AppComponent,
    WritingComponent,
    PublicationComponent,
    ResumeRedirectComponent,
    SoftwareEngineeringComponent,
    HomeComponent,
    PublicationListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatToolbarModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
