export interface Environment {
  resumeUrl: string;
  production: boolean;
}
