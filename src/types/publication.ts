export interface Publication {
  set?: string;
  name: string;
  imageUrl?: string;
  matIcon?: string;
  href?: string;
  routerLink?: string;
  date?: Date; // Optional so we can use this on the SE site.
  desc: string;
}
