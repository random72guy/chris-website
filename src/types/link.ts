export interface Link {
  href: string;
  name: string;
  desc?: string;
  icon?: string;
}
